--Crafts
minetest.register_craft({
   output = 'default:wood 6',
   recipe = {
      {"doors:door_wood"},
   },
})
minetest.register_craft({
   output = 'default:steel_ingot 6',
   recipe = {
       {"doors:door_steel"},
   },
})
minetest.register_craft({
   output = 'default:glass 6',
   recipe = {
      {"doors:door_glass"},
   },
})
minetest.register_craft({
   output = 'default:obsidian_glass 6',
   recipe = {
      {"doors:door_obsidian_glass"},
   },
})
minetest.register_craft({
   output = 'default:wood 3',
   recipe = {
      {"doors:trapdoor"},
   },
})
minetest.register_craft({
   output = 'default:chest_locked',
   recipe = {
      {"default:chest", "default:steel_ingot"},
   },
})

--Aliases
minetest.register_alias("vendor:vendor", "smartshop:shop")
minetest.register_alias("vendor:depositor", "smartshop:shop")
minetest.register_alias("es:desert_stone_with_iron", "default:desert_stone_with_iron")
minetest.register_alias("es:desert_stone_with_gold", "default:desert_stone_with_gold")
minetest.register_alias("es:desert_stone_with_coal", "default:desert_stone_with_coal")
minetest.register_alias("es:stone_with_mese", "default:stone_with_mese")
minetest.register_alias("es:strange_clay_maroon", "cblocks:clay_magenta")
minetest.register_alias("es:strange_clay_black", "cblocks:clay_black")
minetest.register_alias("es:strange_clay_red", "cblocks:clay_red")
minetest.register_alias("es:strange_clay_grey", "cblocks:clay_grey")
minetest.register_alias("es:strange_clay_orange", "cblocks:clay_orange")
minetest.register_alias("es:strange_clay_brown", "cblocks:clay_brown")
minetest.register_alias("protector_mese:protect", "protector:protect")
minetest.register_alias("es:mesecook_crystal", "default:mese_crystal")
minetest.register_alias("es:messymese", "default:mese")
minetest.register_alias("default:rail", "carts:rail")
minetest.register_alias("es:marble", "default:gravel")
minetest.register_alias("mese_crystals:mese_crystal_ore1", "default:stone_with_mese")
minetest.register_alias("mese_crystals:mese_crystal_ore2", "default:stone_with_mese")
minetest.register_alias("mese_crystals:mese_crystal_ore3", "default:stone_with_mese")
minetest.register_alias("mese_crystals:mese_crystal_ore4", "default:stone_with_mese")
minetest.register_alias("carts:minecart", "carts:cart")
minetest.register_alias("mg_villages:torch", "default:torch")
minetest.register_alias("bmail:mailer", "inbox:empty")
