FRISK MOD V0.0.1
-------------

This mod adds a screening device to view other players' inventory.

Screening device
----------------
Sometimes it is necessary to check another player's inventory (e.g. checking for weapons in demilitarized areas, searching for stolen items, verifying consumer solvency etc.). A player with an 'frisk' privilege can use the screening device to see the content of another player's inventory. Stop and frisk them.

      ingot
ingot glass ingot
      ingot

AUTHORS
-------
See file AUTHORS

LICENSE
-------
- code is GPLv3+
- textures are CC-BY-SA 4.0
