--[[
	* If placed close enough to run into a node(s), the lampposts would
	replace (destroy) the node(s). This is due to minetest.set_node being
	used instead of minetest.place_node. However, minetest.place_node won't
	work if the lamppost is placed in a protected area even if the placer is
	the owner of that protected area. So I had to come up with a "Plan B".
	
	Plan B - I've added vertical and horizontal space checks to make sure
	there is enough room for the lamppost. If there isn't, I've also added a
	chat and log message (for troubleshooting) that will tell the player
	that something is in the way and only the first post will be set. The
	rest of the lamppost will not be allowed to form until either the
	obstruction is removed or the lamppost set farther away.

	* Dripping, candle wax animation added to the candle.

	* The large lamp now works with the screwdriver to rotate as the build
	designer wants (inlaid lighting effects).

	~ LazyJ, 2014_07_13

--]]



-- Lantern-Mod by RHR for Minetest 0.49 --
--===========================================

--
-- register nodes:
--
minetest.register_node("lantern:lantern", {
	description = "Lantern",
	drawtype = "nodebox",
	tiles = {"lantern_tb.png","lantern_tb.png","lantern.png","lantern.png","lantern.png","lantern.png"},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = default.LIGHT_MAX-1,
	paramtype2 = "wallmounted",
	walkable = false,
	groups = {snappy = 2, cracky = 2, dig_immediate = 3},
	sounds = default.node_sound_glass_defaults(),
	node_box = {
		type = "wallmounted",
		wall_top = {-1/6, 1/6, -1/6, 1/6, 0.5, 1/6},		
		wall_bottom = {-1/6, -0.5, -1/6, 1/6, -1/6, 1/6}, 	
		wall_side = {-1/6, -1/6, -1/6, -0.5, 1/6, 1/6},
		},
})

minetest.register_node("lantern:fence_black", {
	description = "Black Fence",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, oddly_breakable_by_hand = 3},
	sounds = default.node_sound_defaults(),
	selection_box = {
	type = "fixed",
		fixed = {-1/7, -1/2, -1/7, 1/7, 1/2, 1/7},
		},
})

minetest.register_node("lantern:candle", {
	description = "Candle",
	drawtype = "plantlike",
	inventory_image = "candle_inv.png",
	tiles = {
			{name="candle.png", animation={type = "vertical_frames", aspect_w = 32, aspect_h = 32, length = 3.0}},
		-- "length" was 0.8 but I slowed it down for the dripping wax 
		-- effect I added to the candle's texture file.
		--  ~ LazyJ,2014_05_30
		},	
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	light_source = default.LIGHT_MAX - 1,
	groups = {dig_immediate = 3, attached_node = 1},
	sounds = default.node_sound_defaults(),
	selection_box = {
			type = "fixed",
			fixed = { -0.15, -0.5, -0.15, 0.15, 0.2, 0.15 },
		},
})

minetest.register_node("lantern:lamp", {
	description = "Lamp",
	tiles = {"default_obsidian.png", "default_obsidian.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png"},
	paramtype = "light",
	paramtype2 = "facedir", -- ~ LazyJ
	sunlight_propagates = true,
	walkable = true,
	light_source = default.LIGHT_MAX - 1,
	groups = {snappy = 2, cracky = 2, oddly_breakable_by_hand = 3},
	sounds = default.node_sound_glass_defaults(),
})

--
-- register lampposts in all 4 directions
--
-- See note above. ~ LazyJ, 2014_07_13
minetest.register_node("lantern:lantern_lamppost1", {
	description = "Lamppost 1 (west)",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	inventory_image = "lamppost1.png",
	wield_image = "lamppost_inv.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, dig_immediate = 2},
	sounds = default.node_sound_defaults(),
	after_place_node = function(pos, placer)
		if
		minetest.get_node({x = pos.x-1, y = pos.y, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x-1, y = pos.y + 1, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x-1, y = pos.y + 2, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x-1, y = pos.y + 3, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x-1, y = pos.y + 4, z = pos.z}).name == "air"
		then		
		minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x - 1, y = pos.y + 3, z = pos.z },{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x - 1, y = pos.y + 2, z = pos.z },{name = "lantern:lamp1"})
		else
		minetest.chat_send_player(placer:get_player_name(), "lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		print("lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		end
	end
})


minetest.register_node("lantern:lantern_lamppost2", {
	description = "Lamppost 2 (south)",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	inventory_image = "lamppost2.png",
	wield_image = "lamppost_inv.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, dig_immediate = 2},
	sounds = default.node_sound_defaults(),
	after_place_node = function(pos, placer)
		if
		minetest.get_node({x = pos.x, y = pos.y, z = pos.z-1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 1, z = pos.z-1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 2, z = pos.z-1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 3, z = pos.z-1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 4, z = pos.z-1}).name == "air"
		then
		minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z - 1},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z - 1},{name = "lantern:lamp2"})
		else
		minetest.chat_send_player(placer:get_player_name(), "lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		print("lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		end
	end
})


minetest.register_node("lantern:lantern_lamppost3", {
	description = "Lamppost 3 (east)",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	inventory_image = "lamppost3.png",
	wield_image = "lamppost_inv.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, dig_immediate = 2},
	sounds = default.node_sound_defaults(),
	after_place_node = function(pos, placer)
		if
		minetest.get_node({x = pos.x+1, y = pos.y, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x+1, y = pos.y + 1, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x+1, y = pos.y + 2, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x+1, y = pos.y + 3, z = pos.z}).name == "air" and
		minetest.get_node({x = pos.x+1, y = pos.y + 4, z = pos.z}).name == "air"
		then
		minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x + 1, y = pos.y + 3, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x + 1, y = pos.y + 2, z = pos.z},{name = "lantern:lamp3"})
		else
		minetest.chat_send_player(placer:get_player_name(), "lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		print("lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		end
	end
})


minetest.register_node("lantern:lantern_lamppost4", {
	description = "Lamppost 4 (north)",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	inventory_image = "lamppost4.png",
	wield_image = "lamppost_inv.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, dig_immediate = 2},
	sounds = default.node_sound_defaults(),
	after_place_node = function(pos, placer)
		if
		minetest.get_node({x = pos.x, y = pos.y, z = pos.z+1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 1, z = pos.z+1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 2, z = pos.z+1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 3, z = pos.z+1}).name == "air" and
		minetest.get_node({x = pos.x, y = pos.y + 4, z = pos.z+1}).name == "air"
		then
		minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 3, z = pos.z + 1},{name = "lantern:fence_lamppost"})
		minetest.set_node({x = pos.x, y = pos.y + 2, z = pos.z + 1},{name = "lantern:lamp4"})
		else
		minetest.chat_send_player(placer:get_player_name(), "lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		print("lamppost - Something is in the way. The lamppost needs 4 vertical spaces of air immediately above and to the side where the lamp goes before the whole thing will fit.")
		end
	end
})

--
-- this node is only used  for lamppost and can't be crafted/used by the player
--
minetest.register_node("lantern:fence_lamppost", {
	description = "Fence Lamppost",
	drawtype = "fencelike",
	tiles = {"default_obsidian.png"},
	paramtype = "light",
	is_ground_content = true,
	walkable = true,
	groups = {choppy = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_defaults(),
})

--
-- the nodes lantern:lamp1, lamp2, lamp3 and lamp4 are only used for the lampposts and can't be crafted/used by the player
-- these nodes are also removing the lantern with "after_dig_node"
--
minetest.register_node("lantern:lamp1", {
	description = "Lamp1",
	drop = 'lantern:lantern_lamppost1',
	tiles = {"default_obsidian.png", "default_obsidian.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	light_source = default.LIGHT_MAX - 1,
	groups = {snappy = 2, cracky = 2, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
	type = "fixed",
	fixed = {-0.5, -2.5, -0.5, 1.5, 1.5, 0.5}, 
	},
	after_dig_node = function(pos)
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})	
		minetest.remove_node({x = pos.x + 1, y = pos.y + 1, z = pos.z })	
		minetest.remove_node({x = pos.x + 1, y = pos.y , z = pos.z })
		minetest.remove_node({x = pos.x + 1, y = pos.y - 1, z = pos.z })	
		minetest.remove_node({x = pos.x + 1, y = pos.y - 2, z = pos.z })	
	end
})	

minetest.register_node("lantern:lamp2", {
	description = "Lamp2",
	drop = 'lantern:lantern_lamppost2',
	tiles = {"default_obsidian.png", "default_obsidian.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	light_source = default.LIGHT_MAX - 1,
	groups = {snappy = 2, cracky = 2, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
	type = "fixed",
	fixed = {-0.5, -2.5, -0.5, 0.5, 1.5, 1.5}, 
	},
	after_dig_node = function(pos)
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})	
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z + 1})	
		minetest.remove_node({x = pos.x, y = pos.y , z = pos.z + 1})
		minetest.remove_node({x = pos.x, y = pos.y - 1, z = pos.z + 1})	
		minetest.remove_node({x = pos.x, y = pos.y - 2, z = pos.z + 1})	
	end
})	

minetest.register_node("lantern:lamp3", {
	description = "Lamp3",
	drop = 'lantern:lantern_lamppost3',
	tiles = {"default_obsidian.png", "default_obsidian.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	light_source = default.LIGHT_MAX - 1,
	groups = {snappy = 2, cracky = 2, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
	type = "fixed",
	fixed = {-1.5, -2.5, -0.5, 0.5, 1.5, 0.5}, 
	},
	after_dig_node = function(pos)
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})	
		minetest.remove_node({x = pos.x - 1, y = pos.y + 1, z = pos.z})	
		minetest.remove_node({x = pos.x - 1, y = pos.y , z = pos.z})
		minetest.remove_node({x = pos.x - 1, y = pos.y - 1, z = pos.z})	
		minetest.remove_node({x = pos.x - 1, y = pos.y - 2, z = pos.z})	
	end
})	

minetest.register_node("lantern:lamp4", {
	description = "Lamp4",
	drop = 'lantern:lantern_lamppost4',
	tiles = {"default_obsidian.png", "default_obsidian.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png", "lantern_lamp.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	light_source = default.LIGHT_MAX - 1,
	groups = {snappy = 2, cracky = 2, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
	type = "fixed",
	fixed = {-0.5, -2.5, -1.5, 0.5, 1.5, 0.5}, 
	},
	after_dig_node = function(pos)
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})	
		minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z - 1})	
		minetest.remove_node({x = pos.x, y = pos.y , z = pos.z - 1})
		minetest.remove_node({x = pos.x, y = pos.y - 1, z = pos.z - 1})	
		minetest.remove_node({x = pos.x, y = pos.y - 2, z = pos.z - 1})	
	end
})	

--
-- register crafting recipes:
--
minetest.register_craft({
	output = 'lantern:candle 12',
	recipe = {
		{'default:coal_lump','default:coal_lump'},
		{'group:stick','group:stick'},
		}
})

minetest.register_craft({
	output = 'lantern:lantern 4',
	recipe = {
		{'','group:stick',''},
		{'group:stick','lantern:candle','group:stick'},
		{'','group:stick',''},
		}
})

minetest.register_craft({
	output = 'lantern:lantern 4',
	recipe = {
		{'group:stick','','group:stick'},
		{'','lantern:candle',''},
		{'group:stick','','group:stick'},
		}
})
--[[
	Save this original recipe. I may fork this mod later and add more
	fence-type lanterns (wood, iron, brass) with different designs.

	~ LazyJ

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:fence_black',
	recipe = {'default:coal_lump', 'default:fence_wood'},
})
--]]


--[[
	Custom crafting recipe to fix the issue of having 3 fence types,
	two of which (default:fence_wood and fences:fence_wood) have the
	exact same crafting recipes which prevented the crafting of
	default fences. The third fence type is More Blocks junglewood fence.
	So I added "fences=1" to the other fences and used "group:fences" in the
	recipe.

	~ LazyJ
--]]

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:fence_black',
	recipe = {'default:coal_lump', 'group:fences'},
})



minetest.register_craft({
	output = 'lantern:lamp 4',
	recipe = {
	{'','default:steel_ingot',''},
	{'default:steel_ingot','lantern:candle','default:steel_ingot'},
	{'','default:steel_ingot',''},
	}
})

minetest.register_craft({
	output = 'lantern:lamp 4',
	recipe = {
	{'default:steel_ingot','','default:steel_ingot'},
	{'','lantern:candle',''},
	{'default:steel_ingot','','default:steel_ingot'},
	}
})

minetest.register_craft({
	output = 'lantern:lantern_lamppost1',
	recipe = {
	{'lantern:fence_black','lantern:fence_black',''},
	{'lantern:fence_black','lantern:lamp',''},
	{'lantern:fence_black','',''},
	}
})

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:lantern_lamppost2',
	recipe = {"lantern:lantern_lamppost1"}
})

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:lantern_lamppost3',
	recipe = {"lantern:lantern_lamppost2"}
})

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:lantern_lamppost4',
	recipe = {"lantern:lantern_lamppost3"}
})

minetest.register_craft({
	type = "shapeless",
	output = 'lantern:lantern_lamppost1',
	recipe = {"lantern:lantern_lamppost4"}
})



-- ALIASES to FIX OLD TYPE in LAMPPOST REGISTRATION

minetest.register_alias("lantern:lantern_lampost1", "lantern:lantern_lamppost1")
minetest.register_alias("lantern:lantern_lampost2", "lantern:lantern_lamppost2")
minetest.register_alias("lantern:lantern_lampost3", "lantern:lantern_lamppost3")
minetest.register_alias("lantern:lantern_lampost4", "lantern:lantern_lamppost4")
minetest.register_alias("lantern:fence_lampost", "lantern:fence_lamppost")
